// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package newsman

import (
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"
)

var (
	errArbitraryWebsocketError = errors.New("blah")
)

func TestWebsocketListener_Listen(T *testing.T) {
	T.Parallel()

	T.Run("with error writing JSON", func(t *testing.T) {
		nm := NewNewsman(nil, nil)

		mws := &MockWebsocketConnector{}
		mws.On("SetWriteDeadline", mock.Anything).Return(nil)
		mws.On("WriteJSON", mock.Anything).Return(errArbitraryWebsocketError)
		mws.On("WriteMessage", mock.Anything, mock.Anything).Return(nil)
		mws.On("Close").Return(nil)

		l := NewWebsocketListener(nm, mws, nil)
		go l.Listen()
		c := l.Channel()
		c <- Event{}

		// this is gross and I feel bad
		time.Sleep(250 * time.Millisecond)

		close(c)
	})

	T.Run("when incoming channel has items", func(t *testing.T) {
		nm := NewNewsman(nil, nil)

		mws := &MockWebsocketConnector{}
		mws.On("SetWriteDeadline", mock.Anything).Return(nil)
		mws.On("WriteJSON", mock.Anything).Return(nil)
		mws.On("WriteMessage", mock.Anything, mock.Anything).Return(nil)
		mws.On("Close").Return(nil)

		l := NewWebsocketListener(nm, mws, nil)
		go l.Listen()
		c := l.Channel()

		for i := 0; i < 10; i++ {
			c <- Event{}
		}

		// this is gross and I feel bad
		time.Sleep(250 * time.Millisecond)
	})

	T.Run("when incoming channel has items but errors writing JSON", func(t *testing.T) {
		nm := NewNewsman(nil, nil)

		mws := &MockWebsocketConnector{}
		mws.On("SetWriteDeadline", mock.Anything).Return(nil)
		mws.On("WriteJSON", mock.Anything).Return(errArbitraryWebsocketError)
		mws.On("WriteMessage", mock.Anything, mock.Anything).Return(nil)
		mws.On("Close").Return(nil)

		l := NewWebsocketListener(nm, mws, nil)
		go l.Listen()
		c := l.Channel()
		for i := 0; i < 10; i++ {
			c <- Event{}
		}

		// this is gross and I feel bad
		time.Sleep(250 * time.Millisecond)
	})

	T.Run("when incoming channel has items, but also experiences issues writing JSON", func(t *testing.T) {
		nm := NewNewsman(nil, nil)

		mws := &MockWebsocketConnector{}
		mws.On("SetWriteDeadline", mock.Anything).Return(nil)
		mws.On("WriteJSON", mock.Anything).Return(nil).Once()
		mws.On("WriteJSON", mock.Anything).Return(errArbitraryWebsocketError).Once()
		mws.On("WriteMessage", mock.Anything, mock.Anything).Return(nil)
		mws.On("Close").Return(nil)

		l := NewWebsocketListener(nm, mws, nil)
		go l.Listen()
		c := l.Channel()

		for i := 0; i < 10; i++ {
			c <- Event{}
		}

		// this is gross and I feel bad
		time.Sleep(250 * time.Millisecond)
	})

	T.Run("when incoming channel is closed", func(t *testing.T) {
		nm := NewNewsman(nil, nil)

		mws := &MockWebsocketConnector{}
		mws.On("SetWriteDeadline", mock.Anything).Return(nil)
		mws.On("WriteJSON", mock.Anything).Return(nil)
		mws.On("WriteMessage", mock.Anything, mock.Anything).Return(nil)
		mws.On("Close").Return(nil)

		l := NewWebsocketListener(nm, mws, nil)
		go l.Listen()
		c := l.Channel()
		c <- Event{}
		close(c)

		// this is gross and I feel bad
		time.Sleep(250 * time.Millisecond)
	})
}
