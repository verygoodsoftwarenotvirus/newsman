package newsman

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

// these are all kind of obligatory tests

func TestNewsman_AudienceCount(t *testing.T) {
	t.Parallel()

	nm := NewNewsman(nil, nil)

	assert.Equal(t, uint(0), nm.AudienceCount())
}

func TestNewsman_TuneIn(t *testing.T) {
	t.Parallel()

	nm := NewNewsman(nil, nil)

	l := NewWebhookListener(nil, nil, nil)

	nm.TuneIn(l)
	time.Sleep(50 * time.Microsecond)
	assert.Equal(t, uint(1), nm.AudienceCount())
}

func TestNewsman_TuneOut(t *testing.T) {
	t.Parallel()

	nm := NewNewsman(nil, nil)

	l := NewWebhookListener(nil, nil, nil)

	nm.TuneIn(l)
	time.Sleep(50 * time.Microsecond)
	assert.Equal(t, uint(1), nm.AudienceCount())
	nm.TuneOut(l)
	time.Sleep(50 * time.Microsecond)
	assert.Equal(t, uint(0), nm.AudienceCount())
}

func TestNewsman_Report(t *testing.T) {
	t.Parallel()

	nm := NewNewsman(nil, nil)
	nm.Report(Event{})

}
