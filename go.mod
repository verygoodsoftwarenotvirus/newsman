module gitlab.com/verygoodsoftwarenotvirus/newsman

go 1.13

require (
	github.com/gorilla/websocket v1.4.0
	github.com/sirupsen/logrus v1.4.1 // indirect
	github.com/stretchr/testify v1.3.0
	golang.org/x/tools v0.0.0-20190503185657-3b6f9c0030f7
)
