// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package newsman

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	invalidURL *url.URL
)

func init() {
	var err error
	invalidURL, err = url.Parse("https://www.thisisnotarealwebsiteandasamatteroffactwillneverbeawebsitebecausetheuriismuchtoolongohwelllifegoeson.com")
	if err != nil {
		panic(err)
	}
}

func Test_nilErrFunc(t *testing.T) {
	t.Parallel()

	// this is a petty grab for coverage
	nilErrFunc(nil)
}

func TestWebhookListener_Listen(T *testing.T) {
	T.Parallel()

	T.Run("normal operation", func(t *testing.T) {
		t.Parallel()

		routeHit := make(chan bool)
		srv := httptest.NewServer(
			http.HandlerFunc(
				func(res http.ResponseWriter, req *http.Request) {
					routeHit <- true
				},
			),
		)
		u, _ := url.Parse(srv.URL)

		l := NewWebhookListener(
			nil,
			&WebhookConfig{
				Method:      "GET",
				ContentType: "application/json",
				URL:         u.String(),
			}, nil,
		).(*WebhookListener)

		c := l.Channel()
		for i := 0; i < 10; i++ {
			c <- Event{}
		}
		go l.Listen()

		x := <-routeHit
		assert.True(t, x)
	})

	T.Run("with invalid target", func(t *testing.T) {
		t.Parallel()

		routeHit := make(chan bool)
		srv := httptest.NewServer(
			http.HandlerFunc(
				func(res http.ResponseWriter, req *http.Request) {
					routeHit <- true
				},
			),
		)
		u, _ := url.Parse(srv.URL)

		l := NewWebhookListener(
			nil,
			&WebhookConfig{
				Method:      "GET",
				ContentType: "application/json",
				URL:         u.String(),
			}, nil,
		)
		go l.Listen()
		c := l.Channel()

		for i := 0; i < 10; i++ {
			c <- Event{}
		}
		close(c)

		x := <-routeHit
		assert.True(t, x)

	})

	T.Run("when incoming channel is closed", func(t *testing.T) {
		//t.Parallel()

		l := NewWebhookListener(nil, nil, nil)
		go l.Listen()
		c := l.Channel()
		close(c)
	})
}

func TestWebhookListener_bodyBuilder(T *testing.T) {
	T.Parallel()

	T.Run("normal operation", func(t *testing.T) {
		l := &WebhookListener{
			config: &WebhookConfig{
				ContentType: "application/json",
			},
		}

		v := struct {
			Name string `json:"name"`
		}{
			Name: "JSON",
		}

		l.bodyBuilder(v)
	})

}

func TestWebhookListener_config(T *testing.T) {
	T.Parallel()

	T.Run("normal operation", func(t *testing.T) {
		expected := &ListenerConfig{
			Topics: []string{"blah"},
		}

		l := &WebhookListener{listenerConfig: expected}

		actual := l.Config()
		assert.Equal(t, expected, actual, "expected %v, got %v", expected, actual)
	})

}

func TestWebhookListener_sendWebhook(T *testing.T) {
	T.Parallel()

	T.Run("normal operation", func(t *testing.T) {
		t.Parallel()

		var routeHit bool
		srv := httptest.NewServer(
			http.HandlerFunc(
				func(res http.ResponseWriter, req *http.Request) {
					routeHit = true
				},
			),
		)
		u, _ := url.Parse(srv.URL)

		l := &WebhookListener{
			httpClient: buildDefaultHTTPClient(),
			config: &WebhookConfig{
				Method:      "GET",
				ContentType: "application/json",
				URL:         u.String(),
			},
		}

		err := l.sendWebhook(Event{})
		assert.NoError(t, err)
		assert.True(t, routeHit)
	})

	T.Run("normal operation with bad status code", func(t *testing.T) {
		t.Parallel()

		var (
			routeHit   bool
			errFuncHit bool
		)
		srv := httptest.NewServer(
			http.HandlerFunc(
				func(res http.ResponseWriter, req *http.Request) {
					routeHit = true
					res.WriteHeader(http.StatusBadRequest)
				},
			),
		)
		u, _ := url.Parse(srv.URL)

		exampleErrFunc := func(err error) {
			errFuncHit = true
		}

		l := &WebhookListener{
			httpClient: buildDefaultHTTPClient(),
			config: &WebhookConfig{
				Method:      "GET",
				ContentType: "application/json",
				URL:         u.String(),
			},
			errFunc: exampleErrFunc,
		}

		err := l.sendWebhook(Event{})
		assert.NoError(t, err)
		assert.True(t, routeHit)
		assert.True(t, errFuncHit)
	})

	T.Run("with invalid URL", func(t *testing.T) {
		t.Parallel()

		var routeHit bool
		srv := httptest.NewServer(
			http.HandlerFunc(
				func(res http.ResponseWriter, req *http.Request) {
					routeHit = true
				},
			),
		)
		u, _ := url.Parse(srv.URL)

		l := &WebhookListener{
			httpClient: buildDefaultHTTPClient(),
			config: &WebhookConfig{
				Method:      " ä ö ü ",
				ContentType: "application/json",
				URL:         u.String(),
			},
		}

		err := l.sendWebhook(Event{})
		assert.Error(t, err)
		assert.False(t, routeHit)
	})

	T.Run("with valid URL for nonexistent host", func(t *testing.T) {
		t.Parallel()

		l := &WebhookListener{
			httpClient: buildDefaultHTTPClient(),
			config: &WebhookConfig{
				Method:      "GET",
				ContentType: "application/json",
				URL:         invalidURL.String(),
			},
		}

		assert.Error(t, l.sendWebhook(Event{}))
	})

}
