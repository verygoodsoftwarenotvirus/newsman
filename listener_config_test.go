package newsman

import (
	"net/url"
	"reflect"
	"testing"
)

func TestParseConfigFromURL(t *testing.T) {
	tests := []struct {
		name string
		args url.Values
		want *ListenerConfig
	}{
		{
			name: "normal operation",
			args: url.Values(map[string][]string{
				EventTypesURLKey: {
					"create",
				},
				DataTypesURLKey: {
					"customer",
				},
				TopicsURLKey: {
					"coupon",
				},
			}),
			want: &ListenerConfig{
				Events:    []string{"create"},
				DataTypes: []string{"customer"},
				Topics:    []string{"coupon"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ParseConfigFromURL(tt.args); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseConfigFromURL() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestListenerConfig_Values(t *testing.T) {
	tests := []struct {
		name   string
		config ListenerConfig
		want   url.Values
	}{
		{
			name: "normal operation",
			config: ListenerConfig{
				Events:    []string{"create"},
				DataTypes: []string{"customer"},
				Topics:    []string{"coupon"},
			},
			want: url.Values(map[string][]string{
				EventTypesURLKey: {
					"create",
				},
				DataTypesURLKey: {
					"customer",
				},
				TopicsURLKey: {
					"coupon",
				},
			}),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.config.Values(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ListenerConfig.Values() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_typeString(t *testing.T) {
	tests := []struct {
		name string
		args interface{}
		want string
	}{
		{
			name: "normal operation",
			args: "testing",
			want: "string",
		},
		{
			name: "numbers too",
			args: float64(1),
			want: "float64",
		},
		{
			name: "strips pointer indications",
			args: &ListenerConfig{},
			want: "newsman.ListenerConfig",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := typeString(tt.args); got != tt.want {
				t.Errorf("typeString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestListenerConfig_IsInterested(t *testing.T) {
	tests := []struct {
		name   string
		config *ListenerConfig
		event  Event
		want   bool
	}{
		{
			name: "normal operation expecting all",
			config: &ListenerConfig{
				Events:    []string{"*"},
				DataTypes: []string{"*"},
				Topics:    []string{"*"},
			},
			event: Event{
				EventType: "create",
				Data:      nil,
				Topics:    []string{"holidays"},
			},
			want: true,
		},
		{
			name: "normal operation expecting true",
			config: &ListenerConfig{
				Events:    []string{"create"},
				DataTypes: []string{"float64"},
				Topics:    []string{"holidays"},
			},
			event: Event{
				EventType: "create",
				Data:      float64(1),
				Topics:    []string{"holidays"},
			},
			want: true,
		},
		{
			name: "normal operation expecting false",
			config: &ListenerConfig{
				Events:    []string{"delete"},
				DataTypes: []string{"int32"},
				Topics:    []string{"funerals"},
			},
			event: Event{
				EventType: "create",
				Data:      float64(1),
				Topics:    []string{"holidays"},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.config.IsInterested(tt.event, nil); got != tt.want {
				t.Errorf("ListenerConfig.IsInterested() = %v, want %v", got, tt.want)
			}
		})
	}
}
