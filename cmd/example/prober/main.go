package main

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"

	"github.com/gorilla/websocket"
)

const allQuery = "?event=*&type=*&topic=*"

func checkIfTargetIsUp(addr string) {
	var attempts uint
	for {
		res, err := http.Post(fmt.Sprintf("http://%s/item", addr), "application/json", nil)
		if err != nil || res.StatusCode != http.StatusOK {
			time.Sleep(time.Second)
			attempts++
		} else {
			return
		}

		if attempts >= 25 {
			log.Fatal("target isn't up")
		}
	}

}

// this program is meant to be killed abruptly
func main() {

	var (
		err error

		everySecond = time.NewTicker(time.Second).C
		addr        = os.Getenv("TARGET_ADDRESS")
		wsCount     = os.Getenv("WEBSOCKET_CONNECTIONS")
		minLifetime = os.Getenv("MIN_WEBSOCKET_LIFETIME")
		maxLifetime = os.Getenv("MAX_WEBSOCKET_LIFETIME")
		wsAddr      = fmt.Sprintf("ws://%s/websocket%s", addr, allQuery)
	)

	checkIfTargetIsUp(addr)

	go func() {
		s := http.NewServeMux()
		s.HandleFunc("/", func(res http.ResponseWriter, req *http.Request) {
			log.Println("request received!")
		})

		log.Fatal(http.ListenAndServe(":9090", s))
	}()

	connCount, err := strconv.Atoi(wsCount)
	if err != nil {
		log.Fatal(err)
	}

	maxTime, err := time.ParseDuration(maxLifetime)
	if err != nil {
		log.Fatal(err)
	}

	minTime, err := time.ParseDuration(minLifetime)
	if err != nil {
		minTime = 30 * time.Second
	}

	var connMutex sync.Mutex
	connections := map[*websocket.Conn]time.Time{}

	for i := 0; i < connCount; i++ {
		go func() {
			// log.Printf("attempting to dial: %q", wsAddr)
			conn, _, err := (*websocket.Dialer)(&websocket.Dialer{
				Proxy:            http.ProxyFromEnvironment,
				HandshakeTimeout: 5 * time.Second,
			}).Dial(wsAddr, nil)
			if err != nil {
				log.Fatal(err)
			}

			connMutex.Lock()
			connections[conn] = time.Now().Add(time.Duration(rand.Intn(int(maxTime)) + int(minTime)))
			connMutex.Unlock()
		}()
	}

	for {
		select {
		case <-everySecond:
			go func() {
				for conn, expr := range connections {
					if time.Now().After(expr) {
						connMutex.Lock()
						conn.Close()
						delete(connections, conn)
						connMutex.Unlock()
						continue
					}

					_, _, err := conn.ReadMessage()
					if err != nil {
						log.Println("error reading:", err)
						return
					}
					// log.Printf("received: %s", message)
				}
			}()

			res, err := http.Post(fmt.Sprintf("http://%s/item", addr), "application/json", nil)
			if err != nil || res.StatusCode != http.StatusOK {
				log.Printf("error executing request: %v\n", err)
			}
		}
	}
}
