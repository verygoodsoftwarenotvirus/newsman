package main

import (
	"context"
	"log"
	"net/http"
	"net/url"
	"os"
	"testing"
	"time"

	"gitlab.com/verygoodsoftwarenotvirus/newsman"
)

func newServer() *http.Server {
	u, err := url.Parse(os.Getenv("RECEIVER"))
	if err != nil {
		log.Fatal(err)
	}
	mux := http.NewServeMux()

	authFunc := func(req *http.Request) bool { return true }
	nm := newsman.NewNewsman(authFunc, nil)

	l := newsman.NewWebhookListener(func(err error) {
		log.Printf("error encountered: %v\n", err)
	}, &newsman.WebhookConfig{
		ContentType: "application/json", URL: u.String(),
	}, newsman.AllInclusiveListenerConfig)
	nm.TuneIn(l)

	mux.HandleFunc("/item", func(res http.ResponseWriter, req *http.Request) {
		log.Println("item route hit")
		nm.Report(newsman.Event{
			EventType: "new_item",
			Data: struct {
				Name string `json:"name"`
			}{Name: "hello!"},
			Topics: []string{"new"},
		})
	})

	mux.HandleFunc("/view", func(res http.ResponseWriter, req *http.Request) {
		// inspired by/borrowed from https://gowebexamples.com/websockets/
		_, _ = res.Write([]byte(`
		<html>
			<head></head>
			<body>
				<input id="input" type="text" />
				<button onclick="send()">Send</button>
				<pre id="output"></pre>
				<script>
					var input = document.getElementById("input");
					var output = document.getElementById("output");

					var sp = window.location.search.length === 0 ? "?event=*&type=*&topic=*" : window.location.search;
					var socket = new WebSocket("ws://localhost:7070/websocket"+sp);

					socket.onopen = function () {
						output.innerHTML += "Status: Connected\n";
					};

					socket.onmessage = function (e) {
						output.innerHTML += "Server: " + e.data + "\n";
					};

					function send() {
						socket.send(input.value);
						input.value = "";
					}
				</script>
			</body>
		</html>
		`))
	})

	mux.HandleFunc("/websocket", nm.ServeWebsockets)

	srv := &http.Server{
		Handler: mux,
		Addr:    ":7070",
	}

	go func() {
		// returns ErrServerClosed on graceful close
		if err := srv.ListenAndServe(); err != http.ErrServerClosed {
			// NOTE: there is a chance that next line won't have time to run,
			// as main() doesn't wait for this goroutine to stop.
			os.Exit(0)
		}
	}()

	// returning reference so caller can call Shutdown()
	return srv
}

func main() {
	s := newServer()

	rd := os.Getenv("RUNTIME_DURATION")
	d, err := time.ParseDuration(rd)
	if err != nil {
		log.Fatalf("error parsing %q: %v", rd, err)
	}
	time.Sleep(d)

	if err := s.Shutdown(context.TODO()); err != nil {
		os.Exit(0)
	}
}

func TestRunMain(t *testing.T) {
	main()
}
