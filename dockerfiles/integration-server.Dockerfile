# build stage
FROM golang:alpine AS build-stage

WORKDIR /go/src/gitlab.com/verygoodsoftwarenotvirus/newsman

RUN apk add --update make git gcc musl-dev

ADD . .

RUN go build -o /server -v gitlab.com/verygoodsoftwarenotvirus/newsman/cmd/example/server

# final stage
FROM alpine:latest

COPY --from=build-stage /server /server

EXPOSE 7070

ENTRYPOINT ["/server"]
