# build stage
FROM golang:alpine AS build-stage

WORKDIR /go/src/gitlab.com/verygoodsoftwarenotvirus/newsman

RUN apk add --update make git gcc musl-dev

ADD . .

ENTRYPOINT go run gitlab.com/verygoodsoftwarenotvirus/newsman/cmd/example/prober
