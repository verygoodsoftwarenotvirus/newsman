# build stage
FROM golang:alpine AS build-stage

WORKDIR /go/src/gitlab.com/verygoodsoftwarenotvirus/newsman

RUN apk add --update make git gcc musl-dev

ADD . .

RUN go test -o /coverserver -c -cover -coverpkg="gitlab.com/verygoodsoftwarenotvirus/newsman" gitlab.com/verygoodsoftwarenotvirus/newsman/cmd/testing/coverserver

# final stage
FROM alpine:latest

COPY --from=build-stage /coverserver /coverserver

EXPOSE 7070

ENTRYPOINT ["/coverserver", "-test.coverprofile=/home/integration-coverage.out"]
