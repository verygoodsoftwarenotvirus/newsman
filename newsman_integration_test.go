package newsman

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strconv"
	"sync/atomic"
	"testing"
	"time"

	"github.com/gorilla/websocket"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type person struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
}

const (
	websocketKind              = "websocket"
	webhookKind                = "webhook"
	integrationClientsToSpinUp = 10
)

var (
	allInclusiveListenerConfig = &ListenerConfig{
		Events:    []string{"*"},
		DataTypes: []string{"*"},
		Topics:    []string{"*"},
	}
)

func dialWebsocket(t *testing.T, addr string) *websocket.Conn {
	t.Helper()

	dialer := websocket.DefaultDialer
	u, err := url.Parse(addr)
	require.NoError(t, err)
	u.Scheme = "ws"
	u.RawQuery = allInclusiveListenerConfig.Values().Encode()

	addr = u.String()
	conn, res, err := dialer.Dial(addr, nil)
	if err != nil {
		t.Logf("encountered error dialing %q: %v", addr, err)
		t.FailNow()
		return nil
	}

	if res.StatusCode < http.StatusBadRequest {
		return conn
	}

	t.FailNow()
	return nil
}

func buildPersonFeed(t *testing.T, conn *websocket.Conn, output chan person) {
	t.Helper()
	done := make(chan struct{})
	go func() {
		defer close(done)
		for {
			_, message, err := conn.ReadMessage()
			if err != nil {
				if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
					t.Logf("unexpected websocket closure: %v", err)
					t.FailNow()
				}
				break
			}

			var p person

			if err := json.NewDecoder(bytes.NewReader(message)).Decode(&p); err != nil {
				t.Logf("error decoding item: %v", err)
				t.FailNow()
				break
			}
			output <- p
		}
	}()
}

func buildTestPersonChanFromWebsocket(t *testing.T, serverURL string, nm *Newsman) chan person {
	t.Helper()

	ws := dialWebsocket(t, serverURL)

	personFeed := make(chan person)
	go buildPersonFeed(t, ws, personFeed)

	l := NewWebsocketListener(nm, ws, allInclusiveListenerConfig)
	nm.TuneIn(l)

	return personFeed
}

type testReceiver struct {
	pChan  chan person
	kind   string
	server *httptest.Server
	hits   *numberLog
}

type numberLog struct {
	name  string
	count uint64
}

func (nl *numberLog) Increment(t *testing.T) {
	atomic.AddUint64(&nl.count, 1)
}

func (nl *numberLog) Final() uint64 {
	f := atomic.LoadUint64(&nl.count)
	return f
}

func TestWebsocketsIntegration(t *testing.T) {
	t.Parallel()

	nm := NewNewsman(nil, nil)

	server := httptest.NewServer(http.HandlerFunc(nm.ServeWebsockets))
	defer server.Close()

	var testClients []testReceiver

	for i := 0; i < integrationClientsToSpinUp; i++ {
		if i < integrationClientsToSpinUp/2 {
			testClients = append(testClients, testReceiver{
				kind:  websocketKind,
				pChan: buildTestPersonChanFromWebsocket(t, server.URL, nm),
				hits:  &numberLog{},
			})
		} else {
			hits := &numberLog{name: fmt.Sprintf("webhook%dHitCounter", i)}
			tr := testReceiver{
				kind: webhookKind,
				hits: hits,
				server: httptest.NewServer(
					http.HandlerFunc(
						func(res http.ResponseWriter, req *http.Request) {
							hits.Increment(t)
						},
					),
				),
			}
			defer tr.server.Close()

			u, err := url.Parse(tr.server.URL)
			require.NoError(t, err)

			l := NewWebhookListener(nil, &WebhookConfig{ContentType: "application/json", URL: u.String()}, allInclusiveListenerConfig)
			nm.TuneIn(l)

			testClients = append(testClients, tr)
		}
	}

	personTicker := time.NewTicker(50 * time.Millisecond)
	creationLimit := time.NewTimer(2 * time.Second)
	timeLimit := time.NewTimer(10 * time.Second)

	var (
		eventSent               uint64
		incomplete, createItems = true, true
	)
	for incomplete {
		select {
		case <-personTicker.C:
			if createItems {
				n := strconv.Itoa(int(time.Now().Unix()))
				nm.events <- Event{
					Data: person{FirstName: n, LastName: n},
				}
				atomic.AddUint64(&eventSent, 1)
			}
		// websocket event reporters
		case <-testClients[0].pChan:
			testClients[0].hits.Increment(nil)
		case <-testClients[1].pChan:
			testClients[1].hits.Increment(nil)
		case <-testClients[2].pChan:
			testClients[2].hits.Increment(nil)
		case <-testClients[3].pChan:
			testClients[3].hits.Increment(nil)
		case <-testClients[4].pChan:
			testClients[4].hits.Increment(nil)
		case <-creationLimit.C:
			createItems = false
		case <-timeLimit.C:
			es := atomic.LoadUint64(&eventSent)
			require.NotZero(t, es)

			for i, tc := range testClients {
				hc := tc.hits.Final()

				if tc.kind == websocketKind {
					t.Logf("total number of websocket hits: %d", hc)

					assert.Equal(t, es, hc, "expected the number of hits to websocket %d to be equal to the number of events sent (%d), but there were %d hits to the websocket", i, es, hc)
				} else if tc.kind == webhookKind {
					t.Logf("total number of webhook hits: %d", hc)

					assert.Equal(t, es, hc, "expected the number of hits to webhook %d to be equal to the number of events sent (%d), but there were %d hits to the webhooks", i, es, hc)
				}
			}

			personTicker.Stop()
			timeLimit.Stop()
			incomplete = false
		}
	}
}
